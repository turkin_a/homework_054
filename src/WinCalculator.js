class WinCalculator {
  constructor(cards, typesOfCards) {
    this.cards = cards;
    this.typesOfCards = typesOfCards;
    this.ranks = this.cards.map(card => card.rank);
    this.suits = this.cards.map(card => card.suit);
  }

  checkForHighCard() {
    let highRankIndex = 1;

    for (let rank of this.ranks) {
      if (rank === 'A') {
        highRankIndex = 0;
        break;
      }
      for (let index in this.typesOfCards.ranks) {
        if (rank === this.typesOfCards.ranks[index] && highRankIndex < parseInt(index, 10)) {
          highRankIndex = parseInt(index, 10);
        }
      }
    }

    return highRankIndex;
  };

  checkForStraight() {
    if (this.ranks.includes('A')) {

      if (this.ranks.includes('10') &&
        this.ranks.includes('J') &&
        this.ranks.includes('Q') &&
        this.ranks.includes('K')) {

        if (this.checkForFlush()) return {combination: `Royal Flush!!!`, rate: 100};
        else return {combination: `Straight for "A"`, rate: 4};

      } else if (this.ranks.includes('2') &&
        this.ranks.includes('3') &&
        this.ranks.includes('4') &&
        this.ranks.includes('5')) {

        if (this.checkForFlush()) return {combination: `Straight Flush for "5"`, rate: 50};
        else return {combination: `Straight for "5"`, rate: 4};

      } else return this.checkForFlush();

    } else {
      let highRankIndex = this.checkForHighCard();

      if (this.ranks.includes(this.typesOfCards.ranks[highRankIndex - 1]) &&
        this.ranks.includes(this.typesOfCards.ranks[highRankIndex - 2]) &&
        this.ranks.includes(this.typesOfCards.ranks[highRankIndex - 3]) &&
        this.ranks.includes(this.typesOfCards.ranks[highRankIndex - 4])) {

        if (this.checkForFlush()) return {combination: `Straight Flush for "${this.typesOfCards.ranks[highRankIndex]}"`, rate: 50};
        else return {combination: `Straight for "${this.typesOfCards.ranks[highRankIndex]}"`, rate: 4};

      } else return this.checkForFlush();
    }
  };

  checkForFlush() {
    let suit = this.cards[0].suit;
    let highCard = this.typesOfCards.ranks[this.checkForHighCard()];

    for (let card of this.cards) {
      if (card.suit !== suit) {
        return false;
      }
    }

    return {combination: `Flush with high card "${highCard}"`, rate: 5};
  };

  checkForEquals() {
    let ranksCounter = {};

    this.ranks.forEach(rank => {
      if (!ranksCounter[rank]) ranksCounter[rank] = 1;
      else ranksCounter[rank]++;
    });

    let equals = Object.values(ranksCounter);

    if (equals.length === 5) return this.checkForStraight();

    if (equals.includes(2) && !equals.includes(3)) {
      let pairs = [];

      for (let rank in ranksCounter) {
        if (ranksCounter[rank] === 2) pairs.push(`"${rank}"`);
      }

      let message = ` of ${pairs.join(' and ')}`;

      if (pairs.length === 1) return {combination: `A pair${message}`, rate: 1};
      else return {combination: `Two pairs${message}`, rate: 2};
    }

    if (equals.includes(3)) {
      let set;
      let message;

      for (let rank in ranksCounter) {
        if (ranksCounter[rank] === 3) set = `"${rank}"`;
      }

      message = {combination: `Three of a kind of ${set}`, rate: 3};

      if (equals.includes(2)) {
        let pair;

        for (let rank in ranksCounter) {
          if (ranksCounter[rank] === 2) pair = `"${rank}"`;
        }

        message = {combination: `Full House of ${set} and ${pair}`, rate: 7};
      }
      return message;
    }

    if (equals.includes(4)) {
      for (let rank in ranksCounter) {
        if (ranksCounter[rank] === 4) return {combination: `Four of a kind of "${rank}"`, rate: 20};
      }
    }
  };
}

export default WinCalculator;