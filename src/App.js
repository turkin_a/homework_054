import React, { Component } from 'react';
import './App.css';
import Card from './Card';
import WinCalculator from "./WinCalculator";

const typesOfCards = {
    ranks: ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'],
    suits: ['H', 'C', 'D', 'S']
  };

class App extends Component {
  state = {
    deckOfCards: [],
    cardsOnHand: [],
    currentBet: 10,
    currentCash: 1000,
    winIsGetting: false,
    cardsIsChanged: false
  };

  getCards = () => {
    if (this.state.currentCash < this.state.currentBet) {
      alert('You have not a cash');
      return;
    }

    let currentCash = this.state.currentCash - this.state.currentBet;
    let winIsGetting = false;
    let cardsIsChanged = false;

    let deckOfCards = [];
    let cardsOnHand = [];
    let deck = [];

    for (let suit of typesOfCards.suits) {
      for (let rank of typesOfCards.ranks) {
        let card = {
          rank: rank,
          suit: suit,
          id: `${rank}${suit}`,
          selected: false
        };
        deck.push(card);
      }
    }

    let deckValue = deck.length;

    for (let i = 0; i < deckValue; i++) {
      deckOfCards.push(deck.splice([Math.floor(Math.random() * deck.length)], 1)[0]);
    }

    for (let i = 0; i < 5; i++) {
      cardsOnHand.push(deckOfCards.splice(0, 1)[0]);
    }

    this.setState({deckOfCards, cardsOnHand, currentCash, winIsGetting, cardsIsChanged});
  };

  currentCombination = () => {
    let combination = new WinCalculator(this.state.cardsOnHand, typesOfCards);

    if (this.state.cardsOnHand.length === 0) return '';

    if (combination.checkForEquals()) return combination.checkForEquals();
    else return {combination: `High card is "${typesOfCards.ranks[combination.checkForHighCard()]}"`, rate: 0};
  };

  markAsSelected = (event, id) => {
    const changeBlock = document.getElementById("change");
    changeBlock.className = "invisible";
    const cardsOnHand = [...this.state.cardsOnHand];
    const index = this.state.cardsOnHand.findIndex(c => c.id === id);

    cardsOnHand[index].selected = !cardsOnHand[index].selected;

    for (let key of cardsOnHand) {
      if (key.selected) changeBlock.className = "visible";
    }

    this.setState({cardsOnHand});
  };

  calculateWinnings = () => {
    let currentCash = this.state.currentCash;
    let rate = this.currentCombination().rate;

    if (rate > 0 && !this.state.winIsGetting) {
      currentCash += this.state.currentBet * (rate + 1);
    }

    let winIsGetting = true;

    this.setState({currentCash, winIsGetting});
  };

  changeCards = () => {
    let cardsOnHand = this.state.cardsOnHand;
    let deckOfCards = this.state.deckOfCards;
    let currentCash = this.state.currentCash;

    if (!this.state.cardsIsChanged) {
      currentCash -= this.state.currentBet;

      for (let index in cardsOnHand) {
        if (cardsOnHand[index].selected) {
          cardsOnHand.splice(index, 1, deckOfCards.splice(0, 1)[0]);
        }
      }
    }

    let cardsIsChanged = true;

    this.setState({deckOfCards, cardsOnHand, currentCash, cardsIsChanged});
  };

    render() {
    let cardsOnHand = (
      <div className="playingCards faceImages">
        <ul className="table">
        {
          this.state.cardsOnHand.map((card) => {
            return <Card
              key={card.id}
              rank={card.rank}
              suit={card.suit}
              select={(event) => this.markAsSelected(event, card.id)}
              selected={(card.selected ? 'selected' : '')}
            />
          })
        }
        </ul>
      </div>
    );

    let winRate;
    let totalCash = `Total cash is: ${this.state.currentCash}`;

    if (this.state.cardsOnHand.length === 0) winRate = '';
    else winRate = `Bet is ${this.state.currentBet}. Win rate is `;

    return (
      <div className="App">
        <div>
          <button className="btn" type="button" onClick={this.getCards}>Shuffle cards</button>
          <button className="btn" type="button" onClick={this.calculateWinnings}>Take your winnings</button>
          <span className="total-cash">{totalCash}</span>
          <p>{winRate}<span className="win-rate">{this.currentCombination().rate}</span></p>
          <p>{this.currentCombination().combination}</p>
        </div>
        {cardsOnHand}
        <div id="change" className="invisible">
          <button className="btn" type="button" onClick={this.changeCards}>Change selected cards</button>
          <p>Cost of change cards is <span>{this.state.currentBet}</span></p>
        </div>
      </div>
    );
  }
}

export default App;
