import React from 'react';
import './Card.css';

const suits = {
  'H': {className: 'hearts', symbol: '♥'},
  'C': {className: 'clubs', symbol: '♣'},
  'D': {className: 'diams', symbol: '♦'},
  'S': {className: 'spades', symbol: '♠'}
};

const Card = props => {
  let className = `card rank-${props.rank.toLowerCase()} ${suits[props.suit].className} ${props.selected}`;
  return (
    <div className={className} onClick={props.select}>
      <span className="rank">{props.rank.toUpperCase()}</span>
      <span className="suit">{suits[props.suit].symbol}</span>
    </div>
  );
};

export default Card;